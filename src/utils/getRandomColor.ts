import { DEFAULT_COLORS } from '~/constants/colors';
export const getRandomColor = () => {
  return DEFAULT_COLORS[Math.floor(Math.random() * DEFAULT_COLORS.length)];
};
