// get element position from event
export const getElementPositionFromEvent = (e: MouseEvent) => {
  return {
    x: e.clientX ?? 0,
    y: e.clientY ?? 0
  };
};
