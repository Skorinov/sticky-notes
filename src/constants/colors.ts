/* List of colors used to create a note */
export const DEFAULT_COLORS: string[] = [
  '#CCFFCCFF',
  '#CCCCFFFF',
  '#FFFFCCFF',
  '#FFCCD5FF',
  '#CCD6FFFF',
  '#FFDACCFF',
  '#CCFFFBFF'
];
