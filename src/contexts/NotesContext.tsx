import { createContext, ReactNode, useEffect, useState } from 'react';

import { NOTES_LOCAL_STORAGE_KEY } from '~/constants/keys';
import mockNotes from '~/mocks/notes.json';
import { CreateNoteRequestData, Note, UpdateNoteDataRequestType, UpdateNotePositionRequestType } from '~/types/note';
import { getRandomColor } from '~/utils/getRandomColor';

type NotesContext = {
  notes: Note[];
  createNote: (data: CreateNoteRequestData) => void;
  updateNoteData: (data: UpdateNoteDataRequestType) => void;
  updateNotePosition: (data: UpdateNotePositionRequestType) => void;
  deleteNote: (id: number) => void;
};

type NotesContextProviderPropsType = {
  children: ReactNode;
};

/* The context is intended to manage notes */
export const NotesContext = createContext<NotesContext>({
  notes: [],
  createNote: () => {},
  updateNoteData: () => {},
  deleteNote: () => {},
  updateNotePosition: () => {}
});

/* Hook for managing notes */
const useNotesContextProvider = (): NotesContext => {
  const [notes, setNotes] = useState<Note[]>([]);

  const getDataFromLocalStorage = (): Note[] => {
    try {
      const storageData = localStorage?.getItem(NOTES_LOCAL_STORAGE_KEY);
      if (storageData) {
        return JSON.parse(storageData);
      }
      return [];
    } catch (e) {
      console.log(e);
      return [];
    }
  };

  const updateDataInLocalStorage = (data: Note[]) => {
    try {
      localStorage.setItem(NOTES_LOCAL_STORAGE_KEY, JSON.stringify(data));
    } catch (e) {
      console.log(e);
    }
  };

  // Get notes from local storage if it is empty, get them from mocks
  const getNotes = async () => {
    try {
      let data: Note[] = getDataFromLocalStorage();
      if (!data.length) {
        data = await Promise.resolve(mockNotes);
        updateDataInLocalStorage(data);
      }
      setNotes(data);
    } catch (e) {
      console.log(e);
    }
  };

  const createNote = ({ x = 0, y = 0, width = 200, height = 200 }: CreateNoteRequestData) => {
    const data: Note = {
      id: Date.now(),
      content: 'Note',
      color: getRandomColor(),
      top: y,
      left: x,
      width,
      height
    };
    setNotes((prev) => {
      const newValue = [...prev, data];
      updateDataInLocalStorage(newValue);
      return newValue;
    });
  };

  const updateNotePosition = ({ id, data }: UpdateNotePositionRequestType) => {
    if (!id) {
      return;
    }
    setNotes((prev) => {
      const index = prev.findIndex((item) => item.id === id);
      const value = {
        ...prev[index],
        ...data
      };
      const newValue = [...prev.slice(0, index), ...prev.slice(index + 1), value];
      updateDataInLocalStorage(newValue);
      return newValue;
    });
  };

  const updateNoteData = ({ id, data }: UpdateNoteDataRequestType) => {
    if (!id) {
      return;
    }
    setNotes((prev) => {
      const newValue = prev.map((item) => {
        if (item.id === id) {
          return {
            ...item,
            ...data
          };
        }
        return item;
      });
      updateDataInLocalStorage(newValue);
      return newValue;
    });
  };

  const deleteNote = (id: number) => {
    if (!id) {
      return;
    }
    setNotes((prev) => {
      const newValue = prev.filter((item) => item.id !== id);
      updateDataInLocalStorage(newValue);
      return newValue;
    });
  };

  useEffect(() => {
    getNotes();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    notes,
    createNote,
    updateNoteData,
    updateNotePosition,
    deleteNote
  };
};

/* Provider for wrapping element */
export const NotesContextProvider = ({ children }: NotesContextProviderPropsType) => {
  const value = useNotesContextProvider();
  return <NotesContext.Provider value={value}>{children}</NotesContext.Provider>;
};
