import React from 'react';
import { Footer } from '@components/Footer';
import { Header } from '@components/Header';
import { StickyNotesContainer } from '@components/StickyNotesContainer';

import { NotesContextProvider } from '~/contexts/NotesContext';

import moduleStyles from './App.module.scss';

export const App = () => {
  return (
    <main className={moduleStyles.main}>
      <Header />
      <NotesContextProvider>
        <StickyNotesContainer />
      </NotesContextProvider>
      <Footer />
    </main>
  );
};
