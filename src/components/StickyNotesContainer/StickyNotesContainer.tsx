import React, { useCallback, useContext, useEffect, useRef } from 'react';
import { ContextMenu, ContextMenuItemDataType } from '@components/ContextMenu';
import { MenuItemType } from '@components/Menu';
import { StickyNote } from '@components/StickyNote';

import { NotesContext } from '~/contexts/NotesContext';
import { getElementPositionFromEvent } from '~/utils/getElementPositionFromEvent';

import styles from './StickyNotesContainer.module.scss';

/* This component is designed to display the list of notes */
export const StickyNotesContainer = () => {
  const { notes, createNote } = useContext(NotesContext);
  const refContainer = useRef<HTMLDivElement>(null);
  const startPositionOfSelectedArea = useRef<{ x: number; y: number }>({
    x: 0,
    y: 0
  });
  const isClickedOnly = useRef<boolean>(false);

  const menuItems: MenuItemType[] = [
    {
      type: 'create',
      title: 'Create Note'
    }
  ];

  const onMenuItemClick = ({ item, position }: ContextMenuItemDataType) => {
    if (item.type === 'create') {
      createNote(position);
    }
  };

  const onMouseDown = (e: MouseEvent) => {
    const target = e?.target as HTMLElement;
    if (!target.dataset.card && !target.dataset.resize) {
      isClickedOnly.current = true;
      const { x, y } = getElementPositionFromEvent(e);
      startPositionOfSelectedArea.current = {
        x,
        y
      };
      refContainer.current?.addEventListener('mousemove', onMouseMove);
      refContainer.current?.addEventListener('mouseup', onMouseUp);
    }
  };

  const onMouseMove = useCallback(() => {
    isClickedOnly.current = false;
  }, []);

  const onMouseUp = (e: MouseEvent) => {
    const target = e?.target as HTMLElement;
    if (!target.dataset.card && !target.dataset.resize && !isClickedOnly.current) {
      const { x, y } = getElementPositionFromEvent(e);
      const width = x - startPositionOfSelectedArea.current.x;
      const height = y - startPositionOfSelectedArea.current.y;
      createNote({
        x: startPositionOfSelectedArea.current.x,
        y: startPositionOfSelectedArea.current.y,
        width,
        height
      });
      refContainer.current?.removeEventListener('mousemove', onMouseMove);
    }
  };

  useEffect(() => {
    const container = refContainer.current;

    container?.addEventListener('mousedown', onMouseDown);
    return () => {
      container?.removeEventListener('mousedown', onMouseDown);
      container?.removeEventListener('mousemove', onMouseMove);
      container?.removeEventListener('mouseup', onMouseUp);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ContextMenu menuItems={menuItems} onMenuItemClick={onMenuItemClick}>
      <div ref={refContainer} className={styles.stickyContainer}>
        {notes.map((note) => (
          <StickyNote key={note.id} data={note} />
        ))}
      </div>
    </ContextMenu>
  );
};
