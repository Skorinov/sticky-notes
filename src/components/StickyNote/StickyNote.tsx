import { FocusEvent, useContext, useRef, useState } from 'react';
import { DraggableAndResizableItem, DraggableDataType } from '@components/DraggableAndResizableItem';

import { NotesContext } from '~/contexts/NotesContext';
import { Note } from '~/types/note';

import moduleStyles from './StickyNote.module.scss';

type StickyNotePropsType = {
  data: Note;
};

/* This component is designed to display note and allows to edit text */
export const StickyNote = ({ data }: StickyNotePropsType) => {
  const { updateNoteData, deleteNote, updateNotePosition } = useContext(NotesContext);

  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [content, setContent] = useState<string>(data.content);

  const cardRef = useRef<HTMLDivElement>(null);

  const styles = {
    left: `${data.left}px`,
    top: `${data.top}px`
  };

  const enableEditMode = () => {
    setIsEdit(true);
    setTimeout(() => {
      const cardElement = cardRef?.current;
      if (cardElement) {
        cardElement.focus();
        window?.getSelection()?.selectAllChildren(cardElement);
        window?.getSelection()?.collapseToEnd();
      }
    }, 0);
  };

  const onBlurHandler = (e: FocusEvent<HTMLDivElement>) => {
    const text = e.currentTarget.textContent ?? '';
    if (text !== content) {
      setContent(text);
      updateNoteData({
        id: data.id,
        data: {
          content: text
        }
      });
    }
    setIsEdit(false);
  };

  const onDragEnd = ({ position, dimension, overlapElement }: DraggableDataType) => {
    // if there is overlapElement, remove the note
    if (overlapElement) {
      deleteNote(data.id);
      return;
    }

    const { top, left } = position;
    const { width, height } = dimension;

    if (top !== data.top || left !== data.left) {
      updateNotePosition({
        id: data.id,
        data: {
          top,
          left
        }
      });
    } else if (width !== data.width || height !== data.height) {
      updateNoteData({
        id: data.id,
        data: {
          width,
          height
        }
      });
    }
  };

  return (
    <DraggableAndResizableItem childRef={cardRef} styles={styles} overlapDataAttribute="bin" onDragEnd={onDragEnd}>
      <div
        ref={cardRef}
        className={moduleStyles.stickyCard}
        style={{
          backgroundColor: data.color,
          width: `${data.width}px`,
          height: `${data.height}px`
        }}
        data-card={true}
        contentEditable={isEdit}
        onBlur={onBlurHandler}
        onDoubleClick={enableEditMode}
        suppressContentEditableWarning
      >
        {content}
      </div>
    </DraggableAndResizableItem>
  );
};
