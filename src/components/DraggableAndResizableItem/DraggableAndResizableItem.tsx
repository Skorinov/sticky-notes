import { CSSProperties, ReactNode, RefObject, useCallback, useEffect, useRef } from 'react';

import { getElementPositionFromEvent } from '~/utils/getElementPositionFromEvent';

import moduleStyles from './DraggableAndResizableItem.module.scss';

export type DraggableDataType = {
  position: {
    top: number;
    left: number;
  };
  dimension: {
    width: number;
    height: number;
  };
  overlapElement?: Element;
};

type LastOffsetDataType = {
  top: number;
  left: number;
  startX: number;
  startY: number;
  startChildWidth: number;
  startChildHeight: number;
};

type DraggableItemPropsType = {
  childRef: RefObject<HTMLDivElement>;
  children: ReactNode;
  // provide an attribute to search it in the DOM
  overlapDataAttribute?: string;
  styles: { left: string; top: string } & CSSProperties;
  onDragEnd?: (data: DraggableDataType) => void;
};

const HIGHLIGHTED_STYLE = '0 0 40px 25px rgba(255,255,255,0.70)';
const CURRENT_STYLE_INDEX = '9999';
const DRAGGING_STYLE_INDEX = '99999';

/* This component allows to drag and resize element */
export const DraggableAndResizableItem = ({
  styles,
  children,
  onDragEnd,
  childRef,
  overlapDataAttribute
}: DraggableItemPropsType) => {
  const dragRef = useRef<HTMLDivElement>(null);
  const isDragging = useRef<boolean>(false);
  const isResize = useRef<boolean>(false);
  const lastOffsetData = useRef<LastOffsetDataType>({
    top: 0,
    left: 0,
    startX: 0,
    startY: 0,
    startChildWidth: 0,
    startChildHeight: 0
  });

  // find DOM element in the list of nodes by attribute
  const findOverlapElement = (x: number, y: number) => {
    if (overlapDataAttribute) {
      const elements = document.elementsFromPoint(x, y);
      return elements.find((elem) => !!(elem as HTMLElement)?.dataset?.[overlapDataAttribute]);
    }
  };

  // event fires when the mouse is clicked
  const onDragStartHandler = (e: MouseEvent) => {
    // to determine if a resize or drag event is occurring
    const hasResize = !!(e.target as HTMLDivElement)?.dataset?.resize;
    if (hasResize) {
      isResize.current = true;
    } else {
      isDragging.current = true;
    }

    const dragElement = dragRef.current;

    if (!dragElement) {
      return;
    }

    // be able to drag an element over other elements
    dragElement.style.zIndex = DRAGGING_STYLE_INDEX;

    const { x, y } = getElementPositionFromEvent(e);

    // save the initial offset data
    lastOffsetData.current = {
      top: dragElement.offsetTop - y,
      left: dragElement.offsetLeft - x,
      startX: x,
      startY: y,
      // only child element has width and height
      startChildWidth: childRef?.current?.offsetWidth ?? 0,
      startChildHeight: childRef?.current?.offsetHeight ?? 0
    };

    // add events to track mousemove and mouseup
    dragElement.addEventListener('mouseup', onDragEndHandler);
    document.addEventListener('mousemove', onDragHandler);
  };

  // event fires when the mouse button is released
  const onDragEndHandler = (e: MouseEvent) => {
    isDragging.current = false;
    isResize.current = false;

    if (!dragRef?.current) {
      return;
    }

    const { x, y } = getElementPositionFromEvent(e);

    // reset zIndex for element
    dragRef.current.style.zIndex = CURRENT_STYLE_INDEX;

    if (!isDragging.current && !isResize.current && onDragEnd) {
      // get new position of element
      const { top = 0, left = 0 } = (dragRef.current?.getBoundingClientRect() as DOMRect) ?? {};
      // get new width and height
      const { width = 0, height = 0 } = (childRef?.current?.getBoundingClientRect() as DOMRect) ?? {};
      // to find out if an element is released on top of another element by using attribute
      const overlapElement = findOverlapElement(x, y);

      onDragEnd({
        position: {
          top,
          left
        },
        dimension: {
          width,
          height
        },
        overlapElement
      });
    }
    // remove mousemove event
    document.removeEventListener('mousemove', onDragHandler);
  };

  /*
   * event fires when the mouse moves
   * function is cached to avoid re-rendering
   */
  const onDragHandler = useCallback((e: MouseEvent) => {
    e.preventDefault();

    if (!dragRef.current) {
      return;
    }

    const { x, y } = getElementPositionFromEvent(e);

    // if current element overlaps another element, it is highlighted
    const overlapElement = findOverlapElement(x, y);

    if (overlapElement) {
      dragRef.current.style.boxShadow = HIGHLIGHTED_STYLE;
    } else {
      dragRef.current.style.boxShadow = 'none';
    }

    // if it is a drag event, change the left and top position
    if (isDragging.current) {
      dragRef.current.style.left = x + lastOffsetData.current.left + 'px';
      dragRef.current.style.top = y + lastOffsetData.current.top + 'px';
      return;
    }

    // if it is a resize event, change the width and height of the child element
    if (isResize.current && childRef?.current) {
      childRef.current.style.width = lastOffsetData.current.startChildWidth + x - lastOffsetData.current.startX + 'px';
      childRef.current.style.height =
        lastOffsetData.current.startChildHeight + y - lastOffsetData.current.startY + 'px';
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const dragElement = dragRef.current;

    dragElement?.addEventListener('mousedown', onDragStartHandler);

    return () => {
      dragElement?.removeEventListener('mousedown', onDragStartHandler);
      dragElement?.removeEventListener('mouseup', onDragEndHandler);
      document.removeEventListener('mousemove', onDragHandler);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div ref={dragRef} className={moduleStyles.draggableItem} style={styles}>
      {children}
      <div data-resize={true} className={moduleStyles.resizeBoth}></div>
    </div>
  );
};
