import { CSSProperties, forwardRef } from 'react';

import moduleStyles from './Menu.module.scss';

export type MenuItemType = {
  type: string;
  title: string;
};

type MenuPropsType = {
  items: MenuItemType[];
  onClick?: (item: MenuItemType) => void;
  styles?: CSSProperties;
};
/* This component is designed to display a list of menu items */
export const Menu = forwardRef<HTMLUListElement, MenuPropsType>(({ items = [], styles, onClick }, ref) => {
  const onClickItem = (item: MenuItemType) => {
    onClick?.(item);
  };

  return (
    <ul ref={ref} className={moduleStyles.menu} style={styles}>
      {items.map((item, index) => {
        return (
          <li key={index} className={moduleStyles.menuItem} onClick={() => onClickItem(item)}>
            {item.title}
          </li>
        );
      })}
    </ul>
  );
});
