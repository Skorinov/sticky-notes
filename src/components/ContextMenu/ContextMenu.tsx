import { ReactNode, useCallback, useRef } from 'react';
import { Menu, MenuItemType } from '@components/Menu';

import { useClickOutside } from '~/hooks/useClickOutside';
import { useContextMenu } from '~/hooks/useContextMenu';

import moduleStyles from './ContextMenu.module.scss';

export type ContextMenuItemDataType = {
  item: MenuItemType;
  position: {
    x: number;
    y: number;
  };
};

type ContextMenuPropsType = {
  children: ReactNode;
  menuItems: MenuItemType[];
  onMenuItemClick: (data: ContextMenuItemDataType) => void;
};

/* This component is designed to add and manage a context menu */
export const ContextMenu = ({ menuItems, children, onMenuItemClick }: ContextMenuPropsType) => {
  const menuRef = useRef<HTMLUListElement>(null);
  const { position, visible, ref, setVisible } = useContextMenu<HTMLDivElement>();

  // to cache callback
  const closeModal = useCallback(() => {
    setVisible(false);
  }, [setVisible]);

  useClickOutside(menuRef, closeModal);

  const onItemClick = (item: MenuItemType) => {
    onMenuItemClick?.({
      item,
      position
    });
    closeModal();
  };

  return (
    <div ref={ref} className={moduleStyles.contextMenu}>
      {children}
      {visible && (
        <Menu
          ref={menuRef}
          items={menuItems}
          styles={{
            position: 'fixed',
            left: position.x,
            top: position.y
          }}
          onClick={onItemClick}
        />
      )}
    </div>
  );
};
