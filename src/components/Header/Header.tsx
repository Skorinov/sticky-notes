import moduleStyles from './Header.module.scss';

/* This component is designed to show title and description */
export const Header = () => {
  return (
    <header className={moduleStyles.header}>
      <div>
        <h2>Sticky Notes</h2>
        <p>All data is stored in local storage</p>
        <ul>
          <li>To create a new note, right click and select an action from the context menu.</li>
          <li>
            You can click and drag to create a selection and then release the mouse button to create an element of a
            specific size.
          </li>
          <li>To edit the text, double-click the note, edit and click outside to save the changes</li>
          <li>To drag an element, hold down the left mouse button and move</li>
          <li>To resize an element, pinch the curved end in the lower right corner of the note and move</li>
          <li>To remove an item, drag the item to the bin and release it when the item is highlighted</li>
        </ul>
      </div>
    </header>
  );
};
