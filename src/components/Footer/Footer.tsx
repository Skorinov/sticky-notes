import React from 'react';

import { ReactComponent as BinIcon } from '~/assets/bin.svg';

import moduleStyles from './Footer.module.scss';

/* This component is designed to show bin icon */
export const Footer = () => {
  return <BinIcon data-bin={true} className={moduleStyles.bin} />;
};
