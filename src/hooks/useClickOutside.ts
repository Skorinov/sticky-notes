import { RefObject, useEffect } from 'react';

/* The hook is designed to add and track click event when clicking outside of an element */
export const useClickOutside = <T extends HTMLElement>(ref: RefObject<T>, callback: () => void) => {
  const onClickOutsideHandler = (event: MouseEvent) => {
    const target = event?.target as HTMLElement;
    if (ref?.current && !ref?.current?.contains(target)) {
      callback?.();
    }
  };

  useEffect(() => {
    document.addEventListener('click', onClickOutsideHandler, true);
    return () => {
      document.removeEventListener('click', onClickOutsideHandler, true);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
