import { useEffect, useRef, useState } from 'react';

/* The hook is designed to track contextmenu event and return the mouse position */
export const useContextMenu = <T extends HTMLElement>(initialValue = false) => {
  const ref = useRef<T>(null);
  const [visible, setVisible] = useState(initialValue);
  const [position, setPosition] = useState({
    x: 0,
    y: 0
  });

  const onContextMenuHandler = (e: Event) => {
    const event = e as MouseEvent;
    event.preventDefault();
    setPosition({
      x: event.clientX,
      y: event.clientY
    });
    setVisible(true);
  };

  useEffect(() => {
    const refElement = ref?.current;
    refElement?.addEventListener('contextmenu', onContextMenuHandler);
    return () => {
      refElement?.removeEventListener('contextmenu', onContextMenuHandler);
    };
  }, []);

  return {
    ref,
    visible,
    position,
    setVisible
  };
};
