export type Note = {
  id: number;
  content: string;
  color: string;
  left: number;
  top: number;
  width: number;
  height: number;
};

export type CreateNoteRequestData = {
  x: number;
  y: number;
  width?: number;
  height?: number;
};

export type UpdateNoteDataRequestType = {
  id: number;
  data: {
    content?: string;
    width?: number;
    height?: number;
  };
};

export type UpdateNotePositionRequestType = {
  id: number;
  data: {
    top: number;
    left: number;
  };
};
